import paramiko
import requests
import smtplib
import os

# Set env variables in pycharm
EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')

# Send email message function
def send_notification(email_msg):
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, f"Subject: SITE DONW\n{email_msg}")

# Send email whenever site has different response code then 200 or is completely down
try:
    response = requests.get("https://www.ishish.com")
    if response.status_code == 200:
        print("Application is running successfully")
    else:
        print("App is down")
        msg = f'Application returned {response.status_code}'
        send_notification(msg)

        # restart the application
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(hostname='35.180.205.130', username='root', key_filename='/c/Users/ruan.greyling/.ssh/monitoring.pem')
        # standard input output and error
        stdin, stdout, stderr = ssh.exec_command('docker start <dockerid>')
        print(stdout.readlines())
        ssh.close()
        print('Applicaiton restarted')

except Exception as ex:
    print('Connection error happened')
    msg = 'Not accessible at all'
    send_notification(msg)

    # Restart aws server with boto3


    # Restart application