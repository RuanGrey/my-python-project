import boto3
from operator import itemgetter

ec2_client = boto3.client('ec2', region_name="eu-west-3")

volumes = ec2_client.describe_volumes(
    # Filter to create snapshots only for the production volume
    Filters=[
        {
            'Name': 'tag:Name',
            'Values': ['prod']
        }
    ]
)

for volume in volumes['Volumes']:
    # Gives only your own snapshots not the aws ones
    snapshots = ec2_client.describe_snapshots(
        OwnerIds=['self'],
        Filters=[
            {
                'Name': 'volume-id',
                'Values': [volume['VolumeId']]
            }
        ]
    )

    # Sort the snapshots by time
    sorted_by_date = sorted(snapshots['Snapshots'], key=itemgetter('StartTime'))

    # Skip the first 2 snapshots which were the latest snapshots of the list and delete the rest
    for snap in sorted_by_date[2:]:
        response = ec2_client.delete_snapshot(
            SnapshotId=snap['SnapshotId']
        )